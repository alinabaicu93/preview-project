import {
    createReactProjectGenerator,
} from '@teleporthq/teleport-project-generator-react'
import test2 from "../test2";


export const generateProject = async uidl => {
    try {
        const generator = createReactProjectGenerator()
        const { files, subFolders } = await generator.generateProject(uidl, test2)
        const mappedFiles = mapFiles(subFolders, '')
        files.forEach((file) => {
            mappedFiles[`/${file.name}.${file.fileType}`] = { code: file.content, active: true }
        })
        return mappedFiles
    } catch(e) {
        throw new Error(e);
    }
}

export const mapFiles = (folders, currentPath) => {
    return folders.reduce((acc, folder) => {
        const {files, subFolders} = folder;
        files.map(file => acc[`${currentPath}/${folder.name}/${file.name}.${file.fileType}`] = {code: file.content,})
        if (subFolders.length > 0) {
            acc = { ...acc, ...mapFiles(subFolders, `/${folder.name}`) }
        }
        return acc;
    }, {})
}

