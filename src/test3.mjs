export default {
    name: 'teleport-project-react',
    files: [
        {
            name: 'package',
            content: `
{
  "name": "teleport-project-react",
  "version": "1.0.0",
  "private": true,
  "dependencies": {
    "react": "^17.0.2",
    "react-dom": "^17.0.2",
    "react-router-dom": "^5.2.0",
    "react-scripts": "4.0.3",
    "tailwindcss": "^2.2.7",
    "twind": "^0.16.16"
  },
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test --env=jsdom",
    "eject": "react-scripts eject"
  },
  "browserslist": {
    "production": [
      ">0.2%",
      "not dead",
      "not op_mini all"
    ],
    "development": [
      "last 1 chrome version",
      "last 1 firefox version",
      "last 1 safari version"
    ]
  }
}`,
            fileType: 'json',
        },
        {
            name: "test",
            content: `window.addEventListener(
    "click",
    (e) => {
        e.preventDefault();
        e.stopPropagation();
        console.log("Clicka clicka", e.target.tagName);
        window.parent.postMessage("Click Clock", "http://localhost:3000/");
        if ("BUTTON" === e.target.tagName || "A" === e.target.tagName) {
            console.log(
              "The new color is",
              window.getComputedStyle(e.target).backgroundColor
            );
            e.target.classList.add("test");
        }
    },
    true
);`,
            fileType: 'js',
        },
        {
            name: 'global',
            content: `@tailwind base;
@tailwind components;
@tailwind utilities;`,
            fileType: 'css'
        },
        {
            name: 'tailwind.config',
            content: `const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
    purge: ['./src/**/*.{js,jsx,ts,tsx}'],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            fontFamily: {
                sans: ['Inter var', ...defaultTheme.fontFamily.sans],
            },
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
}`,
            fileType: 'js'
        }
    ],
    subFolders: [],
};