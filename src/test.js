window.addEventListener(
    "click",
    (e) => {
        e.preventDefault();
        e.stopPropagation();
        console.log("Clicka clicka", e.target.tagName);
        window.parent.postMessage("Click Clock", "http://localhost:3000/");
        if ("BUTTON" === e.target.tagName || "A" === e.target.tagName) {
            console.log(
              "The new color is",
              window.getComputedStyle(e.target).backgroundColor
            );
            e.target.classList.add("test");
        }
    },
    true
);