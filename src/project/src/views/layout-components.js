import React from 'react'
import { Link } from 'react-router-dom'

import Helmet from 'react-helmet'

import StickyCard from '../components/sticky-card'
import Stats from '../components/stats'
import Navbar from '../components/navbar'
import ActionCall from '../components/action-call'
import Footer from '../components/footer'
import projectStyles from '../style.module.css'
import styles from './layout-components.module.css'

const LayoutComponents = () => {
  return (
    <div className={styles['layoutcomponents']}>
      <Helmet>
        <title>LayoutComponents - Demo Project</title>
        <meta
          name="description"
          content="This is your initial demo project. Here you'll learn about elements and components and see how to use of them. Please have a look at our Tutorials section for more explanations."
        />
        <meta property="og:title" content="LayoutComponents - Demo Project" />
        <meta
          property="og:description"
          content="This is your initial demo project. Here you'll learn about elements and components and see how to use of them. Please have a look at our Tutorials section for more explanations."
        />
        <meta
          property="og:image"
          content="https://storage.googleapis.com/playground-bucket.teleporthq.io/dd761713af4188367b254d9ce3d8bf08"
        />
      </Helmet>
      <h1
        className={` ${styles['text']} ${projectStyles['c0257c32-9c21-4541-ac16-d634aad5f0f7']} `}
      >
        Layout components
      </h1>
      <div className={styles['grid']}>
        <span className={styles['text1']}>StickyCard</span>
        <div className={styles['stickycard']}>
          <StickyCard></StickyCard>
        </div>
        <span className={styles['text2']}>Navbar</span>
        <div className={styles['stats']}>
          <Stats></Stats>
        </div>
        <span className={styles['text3']}>Stats</span>
        <div className={styles['navbar']}>
          <Navbar></Navbar>
        </div>
        <span className={styles['text4']}>ActionCall</span>
        <ActionCall></ActionCall>
        <span className={styles['text5']}>Footer</span>
        <Footer></Footer>
      </div>
      <div className={styles['navigation']}>
        <h1
          className={` ${styles['text6']} ${projectStyles['c0257c32-9c21-4541-ac16-d634aad5f0f7']} `}
        >
          Navigate to
        </h1>
        <div className={styles['buttons']}>
          <Link
            to="/"
            className={` ${styles['text7']} ${projectStyles['16678ebe-2e77-4c14-bf2f-65d30edaa1c9']} `}
          >
            HOMEPAGE
          </Link>
          <Link
            to="/reusable-components"
            className={` ${styles['text8']} ${projectStyles['16678ebe-2e77-4c14-bf2f-65d30edaa1c9']} `}
          >
            REUSABLE COMPONENTS
          </Link>
        </div>
      </div>
    </div>
  )
}

export default LayoutComponents
