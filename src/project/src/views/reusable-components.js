import React from 'react'
import { Link } from 'react-router-dom'

import Helmet from 'react-helmet'

import SquareCard from '../components/square-card'
import Button from '../components/button'
import Post from '../components/post'
import FooterSection from '../components/footer-section'
import ImageCard from '../components/image-card'
import projectStyles from '../style.module.css'
import styles from './reusable-components.module.css'

const ReusableComponents = () => {
  return (
    <div className={styles['reusablecomponents']}>
      <Helmet>
        <title>ReusableComponents - Demo Project</title>
        <meta
          name="description"
          content="This is your initial demo project. Here you'll learn about elements and components and see how to use of them. Please have a look at our Tutorials section for more explanations."
        />
        <meta property="og:title" content="ReusableComponents - Demo Project" />
        <meta
          property="og:description"
          content="This is your initial demo project. Here you'll learn about elements and components and see how to use of them. Please have a look at our Tutorials section for more explanations."
        />
        <meta
          property="og:image"
          content="https://storage.googleapis.com/playground-bucket.teleporthq.io/dd761713af4188367b254d9ce3d8bf08"
        />
      </Helmet>
      <h1
        className={` ${styles['text']} ${projectStyles['c0257c32-9c21-4541-ac16-d634aad5f0f7']} `}
      >
        Reusable components
      </h1>
      <div className={styles['grid']}>
        <span className={styles['text1']}>SquareCard</span>
        <SquareCard></SquareCard>
        <span className={styles['text2']}>Button</span>
        <Button></Button>
        <span className={styles['text3']}>Post</span>
        <div className={styles['post']}>
          <Post></Post>
        </div>
        <span className={styles['text4']}>FooterSection</span>
        <FooterSection></FooterSection>
        <span className={styles['text5']}>ImageCard</span>
        <div className={styles['imagecard']}>
          <ImageCard></ImageCard>
        </div>
      </div>
      <div className={styles['navigation']}>
        <h1
          className={` ${styles['text6']} ${projectStyles['c0257c32-9c21-4541-ac16-d634aad5f0f7']} `}
        >
          Navigate to
        </h1>
        <div className={styles['buttons']}>
          <Link
            to="/"
            className={` ${styles['text7']} ${projectStyles['16678ebe-2e77-4c14-bf2f-65d30edaa1c9']} `}
          >
            HOMEPAGE
          </Link>
          <Link
            to="/layout-components"
            className={` ${styles['text8']} ${projectStyles['16678ebe-2e77-4c14-bf2f-65d30edaa1c9']} `}
          >
            LAYOUT COMPONENTS
          </Link>
        </div>
      </div>
    </div>
  )
}

export default ReusableComponents
