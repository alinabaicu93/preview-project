import React from 'react'
import { Link } from 'react-router-dom'

import projectStyles from '../style.module.css'
import styles from './action-call.module.css'

const ActionCall = () => {
  return (
    <div className={styles['actioncall']}>
      <span className={styles['text']}>Explore the reusable components.</span>
      <Link
        to="/reusable-components"
        className={` ${styles['text1']} ${projectStyles['16678ebe-2e77-4c14-bf2f-65d30edaa1c9']} `}
      >
        SEE COMPONENTS
      </Link>
    </div>
  )
}

export default ActionCall
