import React from 'react'

const ActionPanel = () => {
  return (
    <div className={` ${'bg-green-300'} ${'shadow'} `}>
      <div className={'text-green-400'}>Explore the reusable components.</div>
    </div>
  )
}

export default ActionPanel
