import { createZipPublisher } from "@teleporthq/teleport-publisher-zip";
import { readFile } from 'fs/promises';
import { createProjectPacker } from "@teleporthq/teleport-project-packer";
import {
    createReactProjectGenerator,
    ReactTemplate,
} from '@teleporthq/teleport-project-generator-react'
import test3 from "./test3.mjs";

const uidl = JSON.parse(await readFile("./uidl.json", "utf8"));

//const gatsbyGenerator = createGatsbyProjectGenerator();
const reactGenerator = createReactProjectGenerator();
const zipPublisher = createZipPublisher({outputPath: '.', projectSlug: 'project'});

const packer = createProjectPacker();
packer.setGenerator(reactGenerator);
packer.setTemplate(test3);
packer.setPublisher(zipPublisher);
const result = await packer.pack(uidl);