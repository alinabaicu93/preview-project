import {
    SandpackLayout,
    SandpackPreview,
    SandpackProvider,
    ClasserProvider,
    SandpackCodeViewer,
    useSandpack,
} from '@codesandbox/sandpack-react';
import {useEffect} from 'react';
import "@codesandbox/sandpack-react/dist/index.css";

const options = {
    uidl: "",
    displayFiles: false,
    dependencies: "",
    files: "",
    theme: "monokai-pro",
}

const Preview = ({options}) => {
    const {displayFiles = false, theme = 'monokai-pro'} = options || {}
    const {sandpack, listen} = useSandpack()

    useEffect(() => {
        sandpack.openInCSBRegisteredRef.current = true
        // const stopListening = listen((message) => console.log("browser", message));
    }, [])

    return (
        <SandpackLayout theme={theme}>
            {displayFiles && <SandpackCodeViewer/>}
            <SandpackPreview showNavigator={true} showOpenInCodeSandbox={false} />
        </SandpackLayout>
    )
}

const BrowserPreview = ({options}) => {
    const {dependencies = {}, files, displayFiles} = options || {}

    return (
        <>
            <SandpackProvider
                bundlerURL="https://sandpack-self-hosted.vercel.app/"
                template="react"
                customSetup={{
                    dependencies,
                    files,
                }}
            >
                <ClasserProvider
                    classes={{
                        'sp-wrapper': 'wrapper',
                        'sp-layout': 'wrapper',
                        'sp-stack': 'custom-stack',
                    }}
                >
                    <Preview options={options}/>
                </ClasserProvider>
            </SandpackProvider>
            <style jsx global>{`
        .wrapper {
          height: 100%;
        }
        .custom-stack.custom-stack {
          min-width: ${displayFiles ? '50% !important' : '100% !important'};
          height: 100%;
        }
      `}</style>
        </>
    )
}

export default BrowserPreview;