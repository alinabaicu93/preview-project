export default {
    name: 'teleport-project-react',
    files: [
        {
            name: 'package',
            content: `
{
  "name": "teleport-project-react",
  "version": "1.0.0",
  "private": true,
  "dependencies": {
    "react": "^17.0.2",
    "react-dom": "^17.0.2",
    "react-router-dom": "^5.2.0",
    "react-scripts": "4.0.3",
    "tailwindcss": "^2.2.7",
    "autoprefixer": "^10.2.6",
    "postcss": "^8.3.6",
    "postcss-cli": "^8.3.1"
  },
  "scripts": {
    "start": "npm run css && react-scripts start",
    "css": "postcss src/global.css -o src/main.css",
    "build": "react-scripts build",
    "test": "react-scripts test --env=jsdom",
    "eject": "react-scripts eject"
  },
  "browserslist": {
    "production": [
      ">0.2%",
      "not dead",
      "not op_mini all"
    ],
    "development": [
      "last 1 chrome version",
      "last 1 firefox version",
      "last 1 safari version"
    ]
  }
}`,
            fileType: 'json',
        },
        {
            name: 'postcss.config',
            content: `module.exports = {
    plugins: [
        require('tailwindcss'),
        require('autoprefixer')
    ],
};
}`,
            fileType: 'js'
        },
    ],
    subFolders: [
        {
            "name": "src",
            "files": [
                {
                    name: "test",
                    content: `window.addEventListener(
    "click",
    (e) => {
        e.preventDefault();
        e.stopPropagation();
        console.log("Clicka clicka", e.target.tagName);
        window.parent.postMessage("Click Clock", "http://localhost:3000/");
        if ("BUTTON" === e.target.tagName || "A" === e.target.tagName) {
            console.log(
              "The new color is",
              window.getComputedStyle(e.target).backgroundColor
            );
            e.target.classList.add("test");
        }
    },
    true
);`,
                    fileType: 'js',
                },
                {
                    name: 'main',
                    content: ``,
                    fileType: 'css'
                },
                {
                    name: 'global',
                    content: `@tailwind base;
@tailwind components;
@tailwind utilities;`,
                    fileType: 'css'
                },
            ],
            "subFolders": []
        },
    ],
}