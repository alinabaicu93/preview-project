import React, {useEffect, useState} from "react";
import BrowserPreview from "./BrowserPreview";
import AppPage from "./AppPage";
import {throttle} from "lodash";
import {generateProject} from "./utils/helpers";

const ProjectPreview = (props) => {
    const [files, setFiles] = useState({})
    const [uidl, setUIDL] = useState();
    const [breakpoint, setBreakpoint] = useState("large");

    const handleUIDLChange = (UIDL) => {
        setUIDL(UIDL)
    }

    const switchBreakpoint = () => {
        switch (breakpoint) {
            case 'small':
                return "small";
            case 'medium':
                return 'medium';
            case 'large':
                return 'large';
            case 'full-screen':
                return "full";
            default:
                return 'large';
        }
    }

    useEffect(() => {
        setUIDL(JSON.stringify(props.uidl, null, 2))
    }, [])

    useEffect(() => {
        const compile = throttle(async () => {
            setFiles({})
            const generatedFiles = await generateProject(JSON.parse(uidl))
            if (!generatedFiles) {
                return
            }
            setFiles(generatedFiles)
        }, 5000)
        compile()
    }, [uidl])


    return (
        <AppPage>
            <div className="project-preview-wrapper">
                <div className="left">
                    <div className="code-wrapper">
                        <textarea name="uidl"
                                  value={uidl}
                                  onChange={e => handleUIDLChange(e.target.value)}
                                  style={{display: 'flex', width: '100%', height: '100vh', textOverflow: 'auto'}}
                        />
                    </div>
                </div>
                <div className="right">
                    {breakpoint !== 'full-screen' ?
                        <div style={{
                            position: 'absolute',
                            zIndex: '1000',
                            bottom: '20px',
                            background: 'lightgreen',
                            padding: ''
                        }}>
                            <img onClick={() => setBreakpoint("small")} src="smartphone.svg" alt=""
                                 style={{height: '30px', width: '30px', zIndex: '1000', margin: '15px'}}/>
                            <img onClick={() => setBreakpoint("medium")} src="tablet.svg" alt=""
                                 style={{height: '30px', width: '30px', zIndex: '1000', margin: '15px'}}/>
                            <img onClick={() => setBreakpoint("large")} src="computer.svg" alt=""
                                 style={{height: '30px', width: '30px', zIndex: '1000', margin: '15px'}}/>
                            <img onClick={() => setBreakpoint("full-screen")} src="full-size.svg" alt=""
                                 style={{height: '30px', width: '30px', zIndex: '1000', margin: '15px'}}/>
                        </div> :
                        <div style={{
                            position: 'absolute',
                            zIndex: '1000',
                            top: '80px',
                            right: '150px',
                            background: 'white',
                            color: 'black',
                            padding: '0 20px',
                            boxShadow: '0px 2px 8px rgba(0, 0, 0, 0.75)',
                            border: '1px solid #EAEDF3'
                        }} className="exit-fs" onClick={() => setBreakpoint("large")}>
                           <p>Exit full screen</p>
                        </div>
                    }
                    <div className={switchBreakpoint()}>
                        {Object.keys(files || {}).length > 0 && (
                            <BrowserPreview options={{files, displayFiles: true}}/>
                        )}
                        {Object.keys(files || {}).length === 0 && (
                            <div className="empty_state">
                                <img className="logo" src="/static/svg/logo_white.svg" alt="Teleport HQ"/>
                                Listening for updates
                            </div>
                        )}
                    </div>
                </div>
            </div>
            <style jsx>
                {`
                
                .small {
                width: 40%;
                height: 100% !important;
                }
                
                .medium {
                width: 60%;
                height: 100% !important;
                }
                
                .large {
                width: 100%;
                height: 100% !important;
                }
               
          .empty_state {
            height: 100%;
            display: flex;
            align-items: center;
            background-size: cover;
            justify-content: center;
            flex-direction: column;
            color: var(--main-text-color);
            font-family: var(--main-font-family);
            font-size: var(--main-text-font-size);
            background-image: url('/static/svg/hero.svg');
          }

          .logo {
            width: 200px;
            padding-bottom: 10px;
          }
          .share-button {
            color: var(--color-purple);
            padding: 6px;
            margin-left: 15px;
            background-color: #fff;
            font-size: 14px;
            border-radius: 4px;
            border: 0 none;
          }

          .editor-header {
            height: 30px;
            display: flex;
            border-bottom: solid 1px var(--editor-bg-black);
            padding: 10px 30px;
            justify-content: space-between;
            align-items: center;
          }

          .code-wrapper {
            height: 100%;
            position: relative;
            overflow: auto;
            background: var(--editor-bg-black);
          }

          .copied-text {
            position: absolute;
            top: 0;
            width: 100%;
            left: 0;
            padding: 5px 0;
            background-color: var(--success-green);
            color: #fff;
            opacity: 0;
          }

          .fade-in {
            animation: fadeInOpacity 1 ease-in 0.35s forwards;
          }

          @keyframes fadeInOpacity {
            0% {
              opacity: 0;
            }
            100% {
              opacity: 1;
            }
          }

          .shareable-link {
            padding: 10px;
            background: rgba(200, 200, 200, 0.5);
            user-select: all;
          }

          .modal-buttons {
            display: flex;
            justify-content: space-between;
            margin: 20px 0 0;
          }

          .modal-button {
            background: var(--color-purple);
            color: #fff;
            padding: 8px 16px;
            font-size: 14px;
            border-radius: 4px;
            border: 0 none;
          }

          .close-button {
            background: rgb(55, 55, 62);
          }

          .project-preview-wrapper {
            display: flex;
            padding: 5px;
            height: calc(100% - 80px);
          }

          .left {
            background-color: var(--editor-bg-black);
            width: 30%;
          }

          .right {
            width: 70%;
            display: flex;
            align-items: center;
            justify-content: center;
            margin-top: -32px;
          }
          
          .full {
          position: absolute;
          height: 100vh;
          width: 100vw;
          top: 0;
          right: 0;
          bottom: 0;
          left:0
          }
          
          .exit-fs:hover {
          cursor: pointer;
          }
          
          .responsive {
          position: absolute;
          background: rgba(RGBA(69, 66, 69, 0.5));
         height: 200px;
         width: 300px;
         display: flex;
         justify-content: center;
         align-items: center;
          }
          
          img:hover {
          cursor: pointer;
          }
        `}
            </style>
        </AppPage>
    )
}

export default ProjectPreview;
